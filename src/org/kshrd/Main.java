package org.kshrd;

 class Student {
    int id;
    String name;
    float score;
    String username;
    String password;

    // Default Constructor
    public Student() {
        System.out.println("Default Constructor");
        //id = 10;
    }
    // Constructor with Parameter
    public Student(int id, String name, float score) {
        this(); // this() will call default constructor
        System.out.println("Constructor with parameter");
        this.id = id;
        this.name = name;
        this.score = score;
    }
    // Constructor with parametter
    public Student(String username, String password) {
         this(2, "KK", 28);
        this.username = username;
        this.password = password;
    }

    public static void main(String[] args) {

        //Student student = new Student(2, "Sokkhet", 20);
        Student student1;
        student1 = new Student();
        System.out.println(student1.id);
    }
}
